{
    "layer": "top", // Waybar at top layer
    "position": "top", // Waybar position (top|bottom|left|right)
    "height": 32, // Waybar height
    // "width": 120, // Waybar width
    // Choose the order of the modules
    "modules-left": ["river/tags", "river/mode", "river/window"],
    "modules-right": ["temperature", "cpu", "battery", "memory", "disk#ssd", "network", "wireplumber",  "backlight", "clock"],    // Modules configuration
    "disk#ssd": {
        "interval": 30,
        "format": "{free}",
        "path": "/",
        "tooltip": true,
        "warning": 80,
        "critical": 90
    },
    "clock": {
        "tooltip-format": "{:%d-%m-%Y | %H:%M}",
        "format-alt": "{:%d-%m-%Y}"
    },
    "cpu": {
        "format": "{usage}% ",
        "on-click": "${TERMINAL} -e htop"
    },
    "memory": {
        "format": "{used}GiB/{total}GiB ",
        "on-click": "${TERMINAL} -e htop"
    },
    "river/window": {
        "max-length": 100
    },
    "temperature": {
        // "thermal-zone": 6,
        "hwmon-path": "/sys/class/hwmon/hwmon7/temp1_input",
        "critical-threshold": 80,
        "format-critical": "{temperatureC}°C ",
        "format": "{temperatureC}°C "
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{percent}% {icon}",
        "states": [0,50],
        "format-icons": ["", ""]
    },
    "battery": {
        "states": {
            "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{capacity}% {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": ["", "", "", "", ""],
        "format-charging": "{capacity}% "

    },
    "network": {
        // "interface": "wlp2s0", // (Optional) To force the use of this interface
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
        "format-disconnected": "Disconnected ⚠",
        "interval" : 7,
        "on-click": "${TERMINAL} -e iwctl"
    },
    "wireplumber": {
        //"scroll-step": 1,
        "format": "{volume}% {icon}",
        "format-muted": "",
        "on-click": "${TERMINAL} -e pulsemixer",
        "format-bluetooth": "{volume}% {icon}",
        "format-icons": {
            "default": ["", ""]
        },
    },
}

// vim:set ft=json et:
