#!/bin/zsh

### If not running interactively, don't do anything
[[ $- != *i* ]] && return

### Options
setopt append_history
setopt auto_cd
setopt auto_pushd
setopt completeinword
setopt correct
setopt extended_glob
setopt extended_history
setopt globstarshort
setopt hash_list_all
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify
setopt histignorespace
setopt inc_append_history
setopt longlistjobs
setopt no_auto_remove_slash
setopt nobeep
setopt nohup
setopt noglobdots
setopt noshwordsplit
unsetopt notify
setopt numericglobsort
setopt prompt_subst
setopt pushd_ignore_dups
setopt unset
stty stop undef

### FPath for lf
fpath=(${XDG_CONFIG_HOME}/lf/_lf ${fpath})
fpath+=(/usr/share/zsh/vendor-completions)

### Autoloads
autoload -Uz add-zsh-hook
autoload -Uz compinit
autoload -Uz complist
autoload -Uz colors
autoload -Uz promptinit
autoload -Uz run-help
autoload -Uz zutil
autoload edit-command-line


### Basic tab completion
zstyle ':completion:*' menu select
zstyle ':completion::complete:*' gain-privileges 1
zmodload zsh/complist

### Activate
compinit
promptinit
colors

### zsh history file; properties and location
HISTFILE="${XDG_CACHE_HOME}/zsh/history"
HISTSIZE=10000
SAVEHIST=10000

### Prompt via ArchWiki
### Empty prompt for starship
### Site for colors: https://robotmoon.com/256-colors/
source /usr/share/git/completion/git-prompt.sh 2>/dev/null
prompt_stas_setup() {
        local almost_theme=`gsettings get org.gnome.desktop.interface gtk-theme`
        local good_theme=${almost_theme:l}
        if [[ $good_theme =~ "light" ]]; then
                PS1='%B%F{#f0b637}%(4~|%3~|%~)%f%F{blue}$(__git_ps1 " (%s)")%f%b $ '
        else
                PS1='%B%F{#ffdf87}%(4~|%3~|%~)%f%F{blue}$(__git_ps1 " (%s)")%f%b $ '
        fi
        RPROMPT='[%F{yellow}%?%f]'
}


### Add theme to promptsys
prompt_themes+=( stas )

### Load the theme
prompt stas

### Include hidden files in autocomplete
_comp_options+=(globdots)

### Rehash after pacman upgrade/install/remove

TRAPUSR1() { rehash }

### Activate V mode
bindkey -v

### Vim keys in tab complete
bindkey -M menuselect '^[[Z' reverse-menu-complete
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

### Edit line in vim with ctrl + e
zle -N edit-command-line
bindkey '^e' edit-command-line
bindkey '^R' history-incremental-pattern-search-backward
bindkey '^S' history-incremental-pattern-search-forward

exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

### QOL
bindkey -r "^j"

alias \
        cp="cpr -v" \
        mv="mvr -v" \
        rsync-ssh="rsync -vhaP" \
        rm="rm -i"


### Change ls to eza
if command -v eza >/dev/null; then
        alias \
                ls="eza -bh --color=auto  --group-directories-first --group --sort=name --git" \
                la="eza -abh --color=auto --group-directories-first --group --sort=name --git" \
                ll="eza -lhb --color=auto --group-directories-first --group --sort=name --git" \
                lt="eza -aT --color=auto --level 2 --group-directories-first --group --sort=name --git"
else
        alias \
                ls="ls -h --color=auto --ignore=.git --ignore=.gitignore --group-directories-first" \
                la="ls -Ah --color=auto --ignore=.git --ignore=.gitignore --group-directories-first" \
                ll="ls -lh --color=auto --ignore=.git --ignore=.gitignore --group-directories-first"
fi

### Other
alias \
        grep="grep --color=auto" \
        ip="ip -color=auto" \
        su="sudo su" \
        cf="change_folder" \
        zathura="zathura --fork" \
        gdb="gdb -q" \
        fzf="fzf --bind 'ctrl-j:down,ctrl-k:up,alt-j:preview-down,alt-k:preview-up'" \
        v="nvim"

### Git
alias \
        gsta="git status" \
        gpul="git pr" \
        gcln="git clone" \
        gadd="git add" \
        gap="git patch" \
        gcom="git ci" \
        gpsh="git push" \
        grem="git restore --staged" \
        gres="git restore" \
        glog="git l" \
        gd="git d" \
        gdd="git d --staged"

alias ssh="TERM=xterm-256color ssh"

alias doom="chocolate-doom"
alias diablo="devilutionx"

cpr() {
        rsync -a -hhh -P --info=stats1,progress2 --modify-window=1 "$@"
}

mvr() {
        rsync -a -hhh -P --info=stats1,progress2 --modify-window=1 --remove-source-files --delete-during "$@"
}

(( ${+aliases[run-help]} )) && unalias run-help
alias help=run-help

#source /usr/share/fzf/key-bindings.zsh 2>/dev/null
source /usr/share/fzf/completion.zsh 2>/dev/null

change_folder() {
        CHOSEN=$(fd -t d -I | fzf --tmux)
        if [[ -z $CHOSEN ]]; then
                echo $CHOSEN
                return 1
        else
                cd $CHOSEN
        fi
        # [[ $(/usr/bin/ls | wc -l) -le 60 ]] && (pwd; ls)
        return 0
}

### Use lf to switch directories and bind it to ctrl-o
lfcd() {
        dir="$(command lf -print-last-dir "$@")"
        [ -d "$dir" ] && [ "$dir" != "$PWD" ] && cd "$dir"
}

function __lfcd_zle {
        zle kill-whole-line;
        lfcd;
        zle reset-prompt;
}

zle -N __lfcd_zle

bindkey '^o' __lfcd_zle

function __cf_zle {
        zle kill-whole-line;
        change_folder;
        zle reset-prompt;
}

function __tmux_zle {
        zle kill-whole-line;
        (
                exec </dev/tty
                exec <&1
                tmux-sessionizer;
        )
        zle reset-prompt;
}

zle -N __cf_zle
bindkey "^f" __cf_zle

zle -N __tmux_zle
bindkey "^t" __tmux_zle

mkcd() {
        mkdir -p "$1" && cd "$1"
}

eval $(dircolors -b)

function osc7-pwd() {
        emulate -L zsh # also sets localoptions for us
        setopt extendedglob
        local LC_ALL=C
        printf '\e]7;file://%s%s\e\' $HOST ${PWD//(#m)([^@-Za-z&-;_~])/%${(l:2::0:)$(([##16]#MATCH))}}
}

function chpwd-osc7-pwd() {
        (( ZSH_SUBSHELL )) || osc7-pwd
}
add-zsh-hook -Uz chpwd chpwd-osc7-pwd

typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
        autoload -Uz add-zle-hook-widget
        function zle_application_mode_start { echoti smkx }
        function zle_application_mode_stop { echoti rmkx }
        add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
        add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

zstyle ':completion:*:approximate:'    max-errors 'reply=( $((($#PREFIX+$#SUFFIX)/3 )) numeric )'
zstyle ':completion:*:complete:-command-::commands' ignored-patterns '(aptitude-*|*\~)'
zstyle ':completion:*:correct:*'       insert-unambiguous true
zstyle ':completion:*:corrections'     format $'%{\e[0;31m%}%d (errors: %e)%{\e[0m%}'
zstyle ':completion:*:correct:*'       original true
zstyle ':completion:*:default'         list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:descriptions'    format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'
zstyle ':completion:*:expand:*'        tag-order all-expansions
zstyle ':completion:*:history-words'   list false
zstyle ':completion:*:history-words'   menu yes
zstyle ':completion:*:history-words'   remove-all-dups yes
zstyle ':completion:*:history-words'   stop yes
zstyle ':completion:*'                 matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*:matches'         group 'yes'
zstyle ':completion:*'                 group-name ''
zstyle ':completion:*:messages'        format '%d'
zstyle ':completion:*:options'         auto-description '%d'
zstyle ':completion:*:options'         description 'yes'
zstyle ':completion:*:processes'       command 'ps -au$USER'
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters
zstyle ':completion:*'                 verbose true
zstyle ':completion:*:-command-:*:'    verbose false
zstyle ':completion:*:warnings'        format $'%{\e[0;31m%}No matches for:%{\e[0m%} %d'
zstyle ':completion:*:*:zcompile:*'    ignored-patterns '(*~|*.zwc)'
zstyle ':completion:correct:'          prompt 'correct to: %e'
zstyle ':completion::(^approximate*):*:functions' ignored-patterns '_*'
zstyle ':completion:*:processes-names' command 'ps c -u ${USER} -o command | uniq'
zstyle ':completion:*:manuals'    separate-sections true
zstyle ':completion:*:manuals.*'  insert-sections   true
zstyle ':completion:*:man:*'      menu yes select
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin \
                                           /usr/local/bin  \
                                           /usr/sbin       \
                                           /usr/bin        \
                                           /sbin           \
                                           /bin

zstyle ':completion:*' special-dirs ..

# run rehash on completion so new installed program are found automatically:
function _force_rehash () {
        (( CURRENT == 1 )) && rehash
        return 1
}

## correction
# some people don't like the automatic correction - so run 'NOCOR=1 zsh' to deactivate it
# try to be smart about when to use what completer...
setopt correct
zstyle -e ':completion:*' completer '
        if [[ $_last_try != "$HISTNO$BUFFER$CURSOR" ]] ; then
                _last_try="$HISTNO$BUFFER$CURSOR"
                reply=(_complete _match _ignored _prefix _files)
        else
                if [[ $words[1] == (rm|mv) ]] ; then
                        reply=(_complete _files)
                else
                        reply=(_oldlist _expand _force_rehash _complete _ignored _correct _approximate _files)
                fi
        fi'

# command for process lists, the local web server details and host completion
zstyle ':completion:*:urls' local 'www' '/var/www/' 'public_html'
### zsh syntax highlighting
### Should be loaded last
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
