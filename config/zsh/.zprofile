#!/bin/zsh

if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
	exec dbus-launch --exit-with-session ssh-agent river >~/.river.log 2>&1
fi
