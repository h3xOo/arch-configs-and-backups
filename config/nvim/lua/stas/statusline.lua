local modes = {
        ["n"]     = "NORMAL",
        ["no"]    = "O-PENDING",
        ["nov"]   = "O-PENDING",
        ["noV"]   = "O-PENDING",
        ["no\22"] = "O-PENDING",
        ["niI"]   = "NORMAL",
        ["niR"]   = "NORMAL",
        ["niV"]   = "NORMAL",
        ["nt"]    = "NORMAL",
        ["ntT"]   = "NORMAL",
        ["v"]     = "VISUAL",
        ["vs"]    = "VISUAL",
        ["V"]     = "V-LINE",
        ["Vs"]    = "V-LINE",
        ["\22"]   = "V-BLOCK",
        ["\22s"]  = "V-BLOCK",
        ["s"]     = "SELECT",
        ["S"]     = "S-LINE",
        ["\19"]   = "S-BLOCK",
        ["i"]     = "INSERT",
        ["ic"]    = "INSERT",
        ["ix"]    = "INSERT",
        ["R"]     = "REPLACE",
        ["Rc"]    = "REPLACE",
        ["Rx"]    = "REPLACE",
        ["Rv"]    = "V-REPLACE",
        ["Rvc"]   = "V-REPLACE",
        ["Rvx"]   = "V-REPLACE",
        ["c"]     = "COMMAND",
        ["cv"]    = "EX",
        ["ce"]    = "EX",
        ["r"]     = "REPLACE",
        ["rm"]    = "MORE",
        ["r?"]    = "CONFIRM",
        ["!"]     = "SHELL",
        ["t"]     = "TERMINAL",
}

local function mode()
        local current_mode = vim.api.nvim_get_mode().mode
        return string.format(" %s ", modes[current_mode]):upper()
end

local function update_mode_colors()
        local current_mode = vim.api.nvim_get_mode().mode
        local mode_color = "%#StatusLineAccent#"
        if current_mode == "n" then
                mode_color = "%#StatuslineAccent#"
        elseif current_mode == "i" or current_mode == "ic" then
                mode_color = "%#StatuslineInsertAccent#"
        elseif current_mode == "v" or current_mode == "V" or current_mode == "\22" then
                mode_color = "%#StatuslineVisualAccent#"
        elseif current_mode == "R" then
                mode_color = "%#StatuslineReplaceAccent#"
        elseif current_mode == "c" then
                mode_color = "%#StatuslineCmdLineAccent#"
        elseif current_mode == "t" then
                mode_color = "%#StatuslineTerminalAccent#"
        end
        return mode_color
end

local function filepath()
        local fpath = vim.fn.fnamemodify(vim.fn.expand "%", ":~:.:h")
        if fpath == "" or fpath == "." then
                return " "
        end

        return string.format(" %%<%s/", fpath)
end

local function filename()
        local fname = vim.fn.expand "%:t"
        if fname == "" then
                return ""
        end
        return fname .. " "
end

local function lsp()
        local count = {}
        local levels = {
                errors = vim.diagnostic.severity.ERROR,
                warnings = vim.diagnostic.severity.WARN,
                info = vim.diagnostic.severity.INFO,
                hints = vim.diagnostic.severity.HINT,
        }

        for k, level in pairs(levels) do
                count[k] = vim.tbl_count(vim.diagnostic.get(0, { severity = level }))
        end

        local errors = ""
        local warnings = ""
        local hints = ""
        local info = ""

        if count["errors"] ~= 0 then
                errors = " %#LspDiagnosticsSignError# " .. count["errors"]
        end
        if count["warnings"] ~= 0 then
                warnings = " %#LspDiagnosticsSignWarning# " .. count["warnings"]
        end
        if count["hints"] ~= 0 then
                hints = " %#LspDiagnosticsSignHint# " .. count["hints"]
        end
        if count["info"] ~= 0 then
                info = " %#LspDiagnosticsSignInformation# " .. count["info"]
        end

        return errors .. warnings .. hints .. info .. "%#Normal#"
end

local function filetype()
        return string.format(" %s ", vim.bo.filetype):upper()
end

local function lineinfo()
        if vim.bo.filetype == "alpha" then
                return ""
        end
        return " %P %l:%c "
end

local M = {}

M.active = function()
        return table.concat {
                "%#Statusline#",
                update_mode_colors(),
                mode(),
                "%#Normal# ",
                filepath(),
                filename(),
                "%#Normal#",
                lsp(),
                "%=%#StatusLineExtra#",
                lineinfo(),
        }
end

M.inactive = function()
        return " %F"
end

M.short = function()
        return "%#StatusLineNC#   neo-tree"
end

local augroup = vim.api.nvim_create_augroup("StatusLine", {})

vim.api.nvim_create_autocmd({ "WinEnter", "BufEnter" }, {
        group = augroup,
        pattern = "*",
        command = "setlocal statusline=%!v:lua.require 'stas.statusline'.active()",
})


vim.api.nvim_create_autocmd({ "WinLeave", "BufLeave" }, {
        group = augroup,
        pattern = "*",
        command = "setlocal statusline=%!v:lua.require 'stas.statusline'.inactive()",
})

vim.api.nvim_create_autocmd({ "WinEnter", "BufEnter", "FileType" }, {
        group = augroup,
        pattern = "neo-tree",
        command = "setlocal statusline=%!v:lua.require 'stas.statusline'.short()",
})

return M
