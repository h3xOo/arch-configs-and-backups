local discipline = require "stas.discipline"

discipline.cowboy()

local key_set = vim.keymap.set

-- Closing buffers/windows
key_set("n", "<leader>Q", "<Cmd>bd<CR>", { desc = "Close buffer" })
key_set("n", "<leader>q", "<Cmd>q<CR>", { desc = "Quit session" })

-- Don't move cursor around
key_set("n", "J", "mzJ`z", { silent = true })
key_set("n", "n", "nzz", { silent = true })
key_set("n", "N", "Nzz", { silent = true })

-- Don't affect registers
key_set({ "n", "v", "x" }, "x", [["_x]])
key_set({ "n", "v", "x" }, "<leader>p", [["0p]])
key_set({ "n", "v", "x" }, "<leader>P", [["0P]])
key_set({ "n", "v", "x" }, "<leader>d", [["_d]])
key_set({ "n", "v", "x" }, "<leader>D", [["_D]])
key_set({ "n", "v", "x" }, "<leader>y", [["+y]])
key_set({ "n", "v", "x" }, "<leader>Y", [["+Y]])

-- QOL
key_set("n", "Q", "<nop>")

key_set("n", "<leader>r", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { desc = "Rename (sed)" })

key_set("n", "<leader>l", "<Cmd>Lazy<CR>", { desc = "Lazy" })

key_set({ "i", "x", "n", "s" }, "<C-s>", "<Cmd>wa<CR>", { desc = "Save file" })

-- Resize window using <ctrl> arrow keys
key_set("n", "<C-Up>", "<Cmd>resize +2<CR>", { desc = "Increase window height" })
key_set("n", "<C-Down>", "<Cmd>resize -2<CR>", { desc = "Decrease window height" })
key_set("n", "<C-Left>", "<Cmd>vertical resize -2<CR>", { desc = "Decrease window width" })
key_set("n", "<C-Right>", "<Cmd>vertical resize +2<CR>", { desc = "Increase window width" })

-- Move Lines
key_set("n", "<A-k>", "<Cmd>m .-2<CR>==", { desc = "Move line up", silent = true })
key_set("i", "<A-k>", "<esc><Cmd>m .-2<CR>==gi", { desc = "Move line up", silent = true })
key_set("v", "<A-k>", ":m '<-2<CR>gv=gv", { desc = "Move line up", silent = true })
key_set("v", "K", ":m '<-2<CR>gv=gv", { desc = "Move line up", silent = true })

key_set("n", "<A-j>", "<Cmd>m .+1<CR>==", { desc = "Move line down", silent = true })
key_set("i", "<A-j>", "<esc><Cmd>m .+1<CR>==gi", { desc = "Move line down", silent = true })
key_set("v", "<A-j>", ":m '>+1<CR>gv=gv", { desc = "Move line down", silent = true })
key_set("v", "J", ":m '>+1<CR>gv=gv", { desc = "Move line down", silent = true })

-- Buffers
key_set("n", "<S-h>", "<Cmd>bprevious<CR>", { desc = "Previous buffer" })
key_set("n", "<S-l>", "<Cmd>bnext<CR>", { desc = "Next buffer" })
key_set("n", "[b", "<Cmd>bprevious<CR>", { desc = "Previous buffer" })
key_set("n", "]b", "<Cmd>bnext<CR>", { desc = "Next buffer" })
key_set("n", "<leader>bD", "<Cmd>bd<CR>", { desc = "Delete buffer and window" })

local function diagnostic_goto(next, severity)
        local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
        severity = severity and vim.diagnostic.severity[severity] or nil
        return function()
                go({ severity = severity })
        end
end

-- Diagnostics jumps
key_set("n", "<leader>cd", vim.diagnostic.open_float, { desc = "Line diagnostics" })
key_set("n", "<C-j>", diagnostic_goto(true), { desc = "Next diagnostic" })
key_set("n", "<C-k>", diagnostic_goto(false), { desc = "Previous diagnostic" })
key_set("n", "]e", diagnostic_goto(true, "ERROR"), { desc = "Next error" })
key_set("n", "[e", diagnostic_goto(false, "ERROR"), { desc = "Previous error" })
key_set("n", "]w", diagnostic_goto(true, "WARN"), { desc = "Next warning" })
key_set("n", "[w", diagnostic_goto(false, "WARN"), { desc = "Previous warning" })

local all_modes = { "i", "s", "n", "c", "v", "o", "x", "l", "t" }

key_set(all_modes, "<C-y>", "<Cmd>delete<CR>")

key_set("n", "]q", vim.cmd.cnext, { desc = "Next quickfix" })
key_set("n", "[q", vim.cmd.cprev, { desc = "Next quickfix" })
