local M = {}

local is_inside_work_tree = {}

local function get_git_opts(initial_mode)
        local is_git = false
        local function is_git_repo()
                vim.fn.system "git rev-parse --is-inside-work-tree"
                return vim.v.shell_error == 0
        end

        local function get_git_root()
                local dot_git_path = vim.fn.finddir(".git", ".;")
                return vim.fn.fnamemodify(dot_git_path, ":h")
        end

        local cwd = vim.fn.getcwd()
        if is_inside_work_tree[cwd] == nil then
                is_inside_work_tree[cwd] = is_git_repo()
        end

        initial_mode = initial_mode or "insert"

        local opts = {
                initial_mode = initial_mode
        }
        if is_inside_work_tree[cwd] then
                opts.cwd = get_git_root()
                is_git = true
        end
        return opts, is_git
end

M.project_files = function()
        local opts, is_git = get_git_opts()
        if is_git then
                require "telescope.builtin".git_files(opts)
        else
                require "telescope.builtin".find_files(opts)
        end
end

M.cwd_files = function()
        local opts, _ = get_git_opts()
        opts.cwd = nil
        require "telescope.builtin".find_files(opts)
end


M.live_grep = function()
        local opts, _ = get_git_opts()
        require "telescope.builtin".live_grep(opts)
end

M.find_files = function()
        local opts, _ = get_git_opts()
        require "telescope.builtin".find_files(opts)
end

M.grep_string = function()
        local opts, _ = get_git_opts("normal")
        require "telescope.builtin".grep_string(opts)
end

return M
