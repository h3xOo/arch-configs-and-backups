local templates_path = vim.fn.stdpath "config" .. "/templates"
local tex_template = templates_path .. "/skeleton.tex"

local create_autocmd = vim.api.nvim_create_autocmd

if vim.uv.fs_stat(tex_template) then
        create_autocmd("BufNewFile", {
                pattern = "*.tex",
                command = "0r " .. tex_template,
        })
end

create_autocmd("FileType", {
        pattern = "ssa",
        command = "syntax off",
})

create_autocmd("InsertLeave", {
        pattern = "*",
        command = "set nopaste",
})

create_autocmd("BufReadPost", {
        pattern = "quickfix",
        command = "nnoremap <buffer> <CR> <CR>",
})

create_autocmd("TextYankPost", {
        pattern = "*",
        command = "silent! lua vim.highlight.on_yank { higroup='Visual', timeout=300 }",
})


local auto_format_on_save_with_filetype = function(pattern)
        create_autocmd("LspAttach", {
                pattern = pattern,
                callback = function(args)
                        local client = vim.lsp.get_client_by_id(args.data.client_id)
                        if not client then
                                return
                        end
                        if client.supports_method "textDocument/formatting" then
                                -- Format the current buffer on save
                                vim.api.nvim_create_autocmd("BufWritePre", {
                                        buffer = args.buf,
                                        callback = function()
                                                vim.lsp.buf.format { bufnr = args.buf, id = client.id }
                                        end
                                })
                        end
                end
        })
end

auto_format_on_save_with_filetype "*.rs"
