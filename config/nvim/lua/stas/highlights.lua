local set_hl = vim.api.nvim_set_hl
local autocmd = vim.api.nvim_create_autocmd

-- The below settings make Leap's highlighting closer to what you've been
-- used to in Lightspeed.

-- Lightspeed colors
-- primary labels: bg = "#f02077" (light theme) or "#ff2f87"  (dark theme)
-- DEPRACTED: secondary labels: bg = "#399d9f" (light theme) or "#99ddff" (dark theme)
-- shortcuts: bg = "#f00077", fg = "white"
-- You might want to use either the primary label or the shortcut colors
-- for Leap primary labels, depending on your taste.

local function leap_dark_bg()
        set_hl(0, "LeapMatch", {
                fg = "white",
                bold = true,
                nocombine = true,
        })
        set_hl(0, "LeapLabel", {
                fg = "white",
                bg = "#ff2f87",
                bold = true,
                nocombine = true,
        })
end

local function leap_light_bg()
        set_hl(0, "LeapMatch", {
                fg = "black",
                bold = true,
                nocombine = true,
        })
        set_hl(0, "LeapLabel", {
                fg = "black",
                bg = "#f02077",
                bold = true,
                nocombine = true,
        })
end

autocmd("ColorScheme", {
        pattern = "*",
        callback = function()
                if vim.g.colors_name == "nord" then
                        return
                end
                local bg = vim.opt.background:get()
                if bg == "dark" then
                        set_hl(0, "LeapBackdrop", { fg = "#999da0" })
                        leap_dark_bg()
                else
                        assert(bg == "light", "If background isn't dark, it should be light")
                        set_hl(0, "LeapBackdrop", { fg = "#767b8d" })
                        leap_light_bg()
                end
        end,
})

local function get_magic_regex(name)
        return "\\v\\c\\@?" .. name .. "(\\(\\w*\\))?(:)?"
end

-- vim.fn.matchadd("DiagnosticInfo", get_magic_regex("todo"))
-- vim.fn.matchadd("DiagnosticError", get_magic_regex("fixme"))
-- vim.fn.matchadd("DiagnosticWarn", get_magic_regex("warning"))
-- vim.fn.matchadd("DiagnosticWarn", get_magic_regex("warn"))
