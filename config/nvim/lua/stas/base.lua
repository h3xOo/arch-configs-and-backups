local set = vim.opt

vim.scriptencoding = "utf-8"
set.encoding = "utf-8"
set.fileencoding = "utf-8"

vim.g.autoformat = false
vim.g.skip_ts_context_commentstring_module = true

set.number = true
set.relativenumber = false
set.pumblend = 0

set.linebreak = true
set.signcolumn = "number"
set.cursorline = true
set.title = true

set.expandtab = true
set.smarttab = true
set.smartindent = true
set.autoindent = true

local tabsize = 8
set.tabstop = tabsize
set.shiftwidth = tabsize
set.softtabstop = tabsize

set.mouse = "a"
set.clipboard:append "unnamedplus"

-- set.colorcolumn = "80"

set.guicursor = ""
set.termguicolors = true

set.path:append "**"
set.wildmenu = true
set.hidden = true

set.hlsearch = true
set.incsearch = true
set.ignorecase = true
set.smartcase = true

set.shell = "zsh"
set.background = "dark"

set.backupskip = { "/tmp/*" }

set.backspace = { "start", "eol", "indent" }
set.showmode = false

vim.cmd [[let &t_Cs = "\e[4:3m"]]
vim.cmd [[let &t_Ce = "\e[4:0m"]]

-- set.formatoptions:append "r"
set.formatoptions = "jcroqlnt"

set.spell = true
set.spelllang:prepend "pl"

set.completeopt = "menu,menuone,noinsert,preview"

local icons = require "stas.icons"

local signs = icons.diagnostics
for type, icon in pairs(signs) do
        local hl = "DiagnosticSign" .. type
        vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

vim.diagnostic.config {
        virtual_text = {
                spacing = 4,
                source = "if_many",
        },
        float = {
                source = true,
        },
        severity_sort = true,
        underline = true,
}

local orig_util_open_floating_preview = vim.lsp.util.open_floating_preview
function vim.lsp.util.open_floating_preview(contents, syntax, opts, ...)
        opts = opts or {}
        opts.border = opts.border or "rounded"
        return orig_util_open_floating_preview(contents, syntax, opts, ...)
end

vim.lsp.set_log_level(vim.lsp.log_levels.OFF)

vim.api.nvim_create_user_command("TrimWhitespace", [[%s/\s\+$//e]], {})
