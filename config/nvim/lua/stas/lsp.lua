local M = {}

local all_modes = { "i", "s", "n", "c", "v", "o", "x", "l", "t" }

M.keymap_opts = function(desc, bufnr)
        return { desc = desc, buffer = bufnr, silent = true, noremap = true }
end

M.make_capabilities = function()
        local capabilities = vim.tbl_deep_extend("force",
                vim.lsp.protocol.make_client_capabilities(),
                require "cmp_nvim_lsp".default_capabilities()
        )

        capabilities.textDocument.foldingRange = {
                dynamicRegistration = false,
                lineFoldingOnly = true,
        }

        return capabilities
end

M.make_clangd_capabilities = function()
        local capabilities = M.make_capabilities()
        capabilities.offsetEncoding = { "utf-16", "utf-8" }

        return capabilities
end

M.on_attach = function(client, bufnr)
        if client == nil then
                return
        end

        if client.name == "ruff" then
                -- Disable hover in favor of Pyright
                client.server_capabilities.hoverProvider = false
        end

        local key_set = vim.keymap.set

        if client.supports_method "textDocument/hover" then
                key_set("n", "K", vim.lsp.buf.hover, M.keymap_opts("Hover", bufnr))
        end

        if client.supports_method "textDocument/implementation" then
                key_set("n", "gi", vim.lsp.buf.implementation, M.keymap_opts("Go to implementations", bufnr))
                -- key_set(all_modes, "<C-A-b>", vim.lsp.buf.implementation, M.keymap_opts("Go to implementations", bufnr))
        end

        if client.supports_method "textDocument/typeDefinition" then
                key_set("n", "gY", vim.lsp.buf.type_definition, M.keymap_opts("Go to t[y]pe definition", bufnr))
                key_set(all_modes, "<C-S-b>", vim.lsp.buf.type_definition,
                        M.keymap_opts("Go to t[y]pe definition", bufnr))
        end

        if client.supports_method "textDocument/references" then
                key_set("n", "gr", vim.lsp.buf.references, M.keymap_opts("Go to references", bufnr))
                key_set(all_modes, "<C-S-F7>", vim.lsp.buf.references, M.keymap_opts("Go to references", bufnr))
                key_set(all_modes, "<C-A-n>", vim.lsp.buf.references, M.keymap_opts("Go to references", bufnr))
        end

        if client.supports_method "textDocument/declaration" then
                key_set("n", "gD", vim.lsp.buf.declaration, M.keymap_opts("Go to declaration", bufnr))
                key_set(all_modes, "<C-b>", vim.lsp.buf.declaration, M.keymap_opts("Go to declaration", bufnr))
        end

        if client.supports_method("textDocument/definition") then
                key_set("n", "gd", vim.lsp.buf.definition, M.keymap_opts("Go to definition", bufnr))
                key_set(all_modes, "<C-A-b>", vim.lsp.buf.definition, M.keymap_opts("Go to implementations", bufnr))
        end

        if client.supports_method "textDocument/signatureHelp" then
                key_set("n", "gK", vim.lsp.buf.signature_help, M.keymap_opts("Signature help", bufnr))
                key_set("i", "<C-k>", vim.lsp.buf.signature_help, M.keymap_opts("Signature help", bufnr))
        end

        if client.supports_method "textDocument/codeAction" then
                key_set({ "n", "v" }, "<A-CR>", vim.lsp.buf.code_action, M.keymap_opts("Code action", bufnr))
        end

        if client.supports_method "textDocument/rename" then
                key_set("n", "<leader>cr", vim.lsp.buf.rename, M.keymap_opts("Rename", bufnr))
                key_set(all_modes, "<S-F6>", vim.lsp.buf.rename, M.keymap_opts("Rename", bufnr))
        end

        if client.supports_method "textDocument/inlayHint" then
                vim.lsp.inlay_hint.enable(false)

                local toggle_inlay_hints = function()
                        vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
                end

                vim.api.nvim_create_user_command("ToggleInlayHints", toggle_inlay_hints, {})

                key_set("n", "<leader>h", toggle_inlay_hints, M.keymap_opts("Toggle inlay hints", bufnr))
        end

        if client.supports_method "textDocument/formatting" then
                key_set({ "n", "v", "i" }, "<C-A-l>", vim.lsp.buf.format, M.keymap_opts("Format file", bufnr))
        end
end

return M
