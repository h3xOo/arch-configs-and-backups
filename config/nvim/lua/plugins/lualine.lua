return {
        "nvim-lualine/lualine.nvim",
        event = "VeryLazy",
        init = function()
                vim.g.lualine_laststatus = vim.o.laststatus
                if vim.fn.argc(-1) > 0 then
                        vim.o.statusline = " "
                else
                        vim.o.laststatus = 0
                end
        end,
        opts = function()
                local lualine_require = require "lualine_require"
                lualine_require.require = require

                local icons = require "stas.icons"

                vim.o.laststatus = vim.g.lualine_laststatus

                return {
                        options = {
                                theme = "auto",
                                globalstatus = vim.o.laststatus == 3,
                                icons_enabled = true,

                                section_separators = { left = "", right = "" },
                                component_separators = { left = "", right = "" },
                        },
                        sections = {
                                lualine_a = {
                                        "mode",
                                },
                                lualine_b = {
                                        "branch",
                                        {
                                                "diff",
                                                symbols = {
                                                        added = icons.git.added,
                                                        modified = icons.git.modified,
                                                        removed = icons.git.removed,
                                                },
                                        },
                                        {
                                                "diagnostics",
                                                sources = { "nvim_diagnostic" },
                                                symbols = {
                                                        error = icons.diagnostics.Error,
                                                        warn  = icons.diagnostics.Warn,
                                                        hint  = icons.diagnostics.Hint,
                                                        info  = icons.diagnostics.Info,
                                                },
                                                colored = true,
                                        },
                                },
                                lualine_c = { { "filename", path = 1 } },
                        },
                }
        end,
}
