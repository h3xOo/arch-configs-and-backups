return {
        "numToStr/Comment.nvim",
        event = "LazyFile",
        dependencies = {
                "JoosepAlviste/nvim-ts-context-commentstring",
                opts = {
                        enable_autocmd = false,
                },
                main = "ts_context_commentstring"
        },
        opts = function()
                return {
                        pre_hook = require "ts_context_commentstring.integrations.comment_nvim".create_pre_hook(),
                }
        end,
}
