vim.api.nvim_create_autocmd("ColorScheme", {
        group = vim.api.nvim_create_augroup("custom_highlights_gruvboxmaterial", {}),
        pattern = "gruvbox-material",
        callback = function()
                local config = vim.fn["gruvbox_material#get_configuration"]()
                local palette = vim.fn["gruvbox_material#get_palette"](config.background, config.foreground,
                        config.colors_override)
                local set_hl = vim.fn["gruvbox_material#highlight"]

                set_hl("TSString", palette.aqua, palette.none, "italic")
                vim.api.nvim_set_hl(0, "@keyword.modifier", {
                        link = "Orange",
                })
        end
})
return {
        "sainnhe/gruvbox-material",
        lazy = false,
        priority = 1500,
        init = function()
                -- Optionally configure and load the colorscheme
                -- directly inside the plugin declaration.
                vim.g.gruvbox_material_background = "hard"
                vim.g.gruvbox_material_foreground = "material"

                vim.g.gruvbox_material_enable_italic = true
                vim.g.gruvbox_material_enable_bold = true

                vim.g.gruvbox_material_line_highlight = true
                vim.g.gruvbox_material_visual = "reverse"
                vim.g.gruvbox_material_sign_column_background = "grey"

                vim.g.gruvbox_material_ui_contrast = "high"

                vim.g.gruvbox_material_diagnostic_text_highlight = true
                vim.g.gruvbox_material_diagnostic_virtual_text = "colored"
                vim.g.gruvbox_material_inlay_hints_background = "dimmed"

                vim.g.gruvbox_material_better_performance = true
        end,
        config = function()
                vim.cmd.colorscheme "gruvbox-material"
        end
}
