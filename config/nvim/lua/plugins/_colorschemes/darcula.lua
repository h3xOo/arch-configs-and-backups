return {
        -- "santos-gabriel-dario/darcula-solid.nvim",
        -- dependencies = "rktjmp/lush.nvim",
        -- lazy = false,
        -- priority = 1500,
        -- config = function ()
        --         vim.cmd.colorscheme "darcula-solid"
        -- end
        -- "doums/darcula",
        -- lazy = false,
        -- priority = 1500,
        -- config = function ()
        --         vim.cmd.colorscheme "darcula"
        -- end
        "xiantang/darcula-dark.nvim",
        lazy = false,
        priority = 1500,
        opts = {
                opt = {
                        integrations = {
                                telescope = false,
                        },
                },
        },
        config = function(_, opts)
                require "darcula".setup(opts)
                vim.api.nvim_set_hl(0, "TSTypeBuiltin", { fg = "#8888c6" })
                vim.api.nvim_set_hl(0, "TSConstant", { fg = "#94558d" })
                vim.api.nvim_set_hl(0, "TSFunction", { fg = "#ffc66d" })
                vim.api.nvim_set_hl(0, "TSString", { fg = "#6a8759", italic = true })
                vim.api.nvim_set_hl(0, "TSNumber", { fg = "#6897bb" })
                vim.api.nvim_set_hl(0, "TSVariableBuiltin", { link = "TSConstant" })
                vim.api.nvim_set_hl(0, "TSFuncBuiltin", { link = "TSConstant" })

                vim.api.nvim_set_hl(0, "@string.documentation", { fg = "#629755", italic = true })
                vim.api.nvim_set_hl(0, "@lsp.type.comment", { link = "TSComment" })
                vim.api.nvim_set_hl(0, "@lsp.type.function", { link = "TSFunction" })
                vim.api.nvim_set_hl(0, "@lsp.type.method", { link = "TSMethod" })
        end
        -- "RRethy/base16-nvim",
        -- lazy = false,
        -- priority = 1500,
        -- config = function(_, opts)
        --         -- require "base16-colorscheme".setup(opts)
        --         vim.cmd.colorscheme "base16-darcula"
        -- end,
        -- "AlexvZyl/nordic.nvim",
        -- lazy = false,
        -- priority = 1500,
        -- config = function()
        --         require "nordic".load()
        -- end
}
