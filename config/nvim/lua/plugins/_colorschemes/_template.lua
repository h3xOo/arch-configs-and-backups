local M = {}

M.setup_scheme = function(repo, require_name, opts, lazy_name)
        local ret = {
                repo,
                priority = 1500,
                lazy = false,
                opts = opts,
                config = function(_, opts_)
                        require(require_name).setup(opts_)
                        vim.cmd.colorscheme(require_name)
                end,
        }

        if lazy_name ~= nil then
                ret.name = lazy_name
        end
        return ret
end

return M
