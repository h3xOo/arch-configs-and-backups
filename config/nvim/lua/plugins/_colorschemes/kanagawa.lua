return require "plugins._colorschemes._template".setup_scheme("rebelot/kanagawa.nvim", "kanagawa", {
        compile = true,
        background = {
                dark = "dragon",
        },
        overrides = function(colors)
                local theme = colors.theme

                local make_diagnostic_color = function(color)
                        local c = require("kanagawa.lib.color")
                        return { fg = color, bg = c(color):blend(theme.ui.bg, 0.95):to_hex() }
                end

                return {
                        String                     = { fg = theme.syn.string, italic = true },
                        ["@lsp.type.comment"]      = { link = "Comment" },

                        DiagnosticVirtualTextHint  = make_diagnostic_color(theme.diag.hint),
                        DiagnosticVirtualTextInfo  = make_diagnostic_color(theme.diag.info),
                        DiagnosticVirtualTextWarn  = make_diagnostic_color(theme.diag.warning),
                        DiagnosticVirtualTextError = make_diagnostic_color(theme.diag.error),
                }
        end,
})
