return require "plugins._colorschemes._template".setup_scheme("ellisonleao/gruvbox.nvim", "gruvbox",
        function()
                return {
                        invert_selection = true,
                        invert_tabline = true,
                        overrides = {
                                ["@keyword.modifier"] = { link = "GruvboxOrange" },
                                Directory = { link = "GruvboxBlueBold" },
                        },
                        contrast = "hard",
                }
        end)
