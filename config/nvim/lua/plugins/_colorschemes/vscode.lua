return {
        "Mofiqul/vscode.nvim",
        lazy = false,
        priority = 1500,
        config = function()
                vim.cmd.colorscheme "vscode"
        end
}
