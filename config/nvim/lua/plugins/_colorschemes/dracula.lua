return require "plugins._colorschemes._template".setup_scheme("Mofiqul/dracula.nvim", "dracula", {
        overrides = function(colors)
                return {
                        SpellBad = { undercurl = true, sp = colors.bright_red },
                        SpellCap = { undercurl = true, sp = colors.bright_cyan },
                        SpellLocal = { undercurl = true, sp = colors.bright_cyan },
                        SpellRare = { undercurl = true, sp = colors.bright_blue },
                }
        end,
})

-- return {
--         "dracula/vim",
--         name = "dracula",
--         priority = 1500,
--         lazy = false,
--         config = function ()
--                 vim.cmd.colorscheme "dracula"
--         end
-- }
