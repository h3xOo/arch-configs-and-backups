return require "plugins._colorschemes._template".setup_scheme("catppuccin/nvim", "catppuccin", {
        background = {
                light = "latte",
                dark = "mocha",
        },
}, "catppuccin")
