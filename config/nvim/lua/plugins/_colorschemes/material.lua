return require "plugins._colorschemes._template".setup_scheme("marko-cerovac/material.nvim", "material",
        function()
                if vim.opt.background:get() == "dark" then
                        vim.g.material_style = "oceanic"
                else
                        vim.g.material_style = "lighter"
                end
                return {
                        styles = {
                                strings = { italic = true },
                        },
                        plugins = {
                                -- "dap",
                                "gitsigns",
                                "neo-tree",
                                "nvim-cmp",
                                "nvim-web-devicons",
                                "telescope",
                                "trouble",
                        },
                        disable = {
                                colored_cursor = true,
                        },
                        lualine_style = "stealth",
                        custom_highlights = {
                                SpellBad = function(colors, _)
                                        return {
                                                undercurl = true,
                                                sp = colors.main.red,
                                        }
                                end,
                                SpellCap = function(colors, _)
                                        return {
                                                undercurl = true,
                                                sp = colors.main.blue,
                                        }
                                end,

                                SpellLocal = function(colors, _)
                                        return {
                                                undercurl = true,
                                                sp = colors.main.cyan,
                                        }
                                end,

                                SpellRare = function(colors, _)
                                        return {
                                                undercurl = true,
                                                sp = colors.main.purple,
                                        }
                                end,
                        },
                }
        end)
