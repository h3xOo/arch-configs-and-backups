return {
        -- "calind/selenized.nvim",
        -- lazy = false,
        -- priority = 1500,
        -- config = function()
        --         vim.cmd.colorscheme "selenized"
        -- end
        -- "askfiy/visual_studio_code",
        -- priority = 1500,
        -- config = function()
        --         vim.cmd.colorscheme "visual_studio_code"
        -- end,
        -- "mellow-theme/mellow.nvim",
        -- lazy = false,
        -- priority = 1500,
        -- config = function ()
        --         vim.cmd.colorscheme "mellow"
        -- end
        "dgox16/oldworld.nvim",
        lazy = false,
        priority = 1000,
        opts = true,
        config = function (_, opts)
                require "oldworld".setup(opts)
                vim.cmd.colorscheme "oldworld"
        end
        -- "xero/miasma.nvim",
        -- lazy = false,
        -- priority = 1500,
        -- config = function()
        --         vim.cmd.colorscheme "miasma"
        -- end,
}
