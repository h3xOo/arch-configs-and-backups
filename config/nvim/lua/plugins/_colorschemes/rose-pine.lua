return require "plugins._colorschemes._template".setup_scheme("rose-pine/neovim", "rose-pine", {
        highlight_groups = {
                string = { fg = "gold", italic = true },
                boolean = { fg = "rose", italic = true },
                ["@function"] = { fg = "rose", italic = true },
                ["@property"] = { fg = "iris" },
                Keyword = { fg = "pine", italic = true },
                Macro = { fg = "iris", bold = true },
                ["@constant.macro"] = { fg = "gold", bold = true },
                ["@type.builtin"] = { fg = "foam", bold = false },
                ["@function.builtin"] = { fg = "rose", bold = false },
        },
        styles = {
                italic = false,
        },
}, "rose-pine")
