return require "plugins._colorschemes._template".setup_scheme("gbprod/nord.nvim", "nord",
        function()
                return {
                        errors = { mode = "fg" },
                        search = { theme = "vscode" },
                        on_highlights = function(highlights, colors)
                                highlights["WarningMsg"] = { fg = colors.aurora.yellow }
                                highlights["ErrorMsg"] = { fg = colors.aurora.red }
                        end,
                }
        end)
