---@type string?
local build_cmd
for _, cmd in ipairs { "cmake", "make", "gmake" } do
        if vim.fn.executable(cmd) == 1 then
                build_cmd = cmd
                break
        end
end

return {
        "nvim-telescope/telescope.nvim",
        version = false,
        dependencies = {
                {
                        "nvim-telescope/telescope-fzf-native.nvim",
                        enabled = build_cmd ~= nil,
                        build = (build_cmd ~= "cmake") and "make" or
                            "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
                },
                "nvim-telescope/telescope-file-browser.nvim",
                {
                        "MunifTanjim/nui.nvim",
                        lazy = true
                }
        },
        cmd = "Telescope",
        keys = {
                {
                        "<localleader><localleader>",
                        function()
                                require "telescope.builtin".buffers()
                        end,
                        desc = "Buffers",
                        silent = true,
                        noremap = true,
                },
                {
                        "<localleader>s",
                        require "stas.telescope-config".grep_string,
                        desc = "Grep string",
                        silent = true,
                        noremap = true,
                },
                {
                        "<localleader>b",
                        function()
                                local telescope = require "telescope"
                                local function telescope_buffer_dir()
                                        return vim.fn.expand "%:p:h"
                                end

                                telescope.extensions.file_browser.file_browser {
                                        path = "%:p:h",
                                        cwd = telescope_buffer_dir(),
                                        respect_gitignore = false,
                                        hidden = true,
                                        grouped = true,
                                        previewer = false,
                                        layout_config = { height = 40 },
                                }
                        end,
                        desc = "File browser",
                        silent = true,
                        noremap = true,
                },
                {
                        "<C-A-p>",
                        require "stas.telescope-config".project_files,
                        desc = "Find project files",
                        silent = true,
                        noremap = true
                },
                {
                        "<C-p>",
                        require "stas.telescope-config".cwd_files,
                        desc = "Find cwd files",
                        silent = true,
                        noremap = true
                },
                {
                        "<localleader>r",
                        require "stas.telescope-config".live_grep,
                        desc = "Live grep",
                        silent = true,
                        noremap = true
                },
                {
                        "<localleader>t",
                        "<Cmd>Telescope builtin<CR>",
                        desc = "Telescope",
                        silent = true,
                        noremap = true
                },
                {
                        "<C-q>",
                        "<Cmd>Telescope quickfix initial_mode=normal<CR>",
                        desc = "Open quickfix list in telescope",
                        silent = true,
                        noremap = true,
                },
        },
        config = function()
                local telescope = require "telescope"
                local actions = require "telescope.actions"
                local action_layout = require "telescope.actions.layout"
                local config = require "telescope.config"
                local fb_actions = require "telescope".extensions.file_browser.actions

                local vimgrep_arguments = { unpack(config.values.vimgrep_arguments) }
                table.insert(vimgrep_arguments, "--trim")

                telescope.setup {
                        defaults = {
                                mappings = {
                                        n = {
                                                ["q"] = "close",
                                                ["<C-c>"] = "close",
                                                ["<M-p>"] = action_layout.toggle_preview
                                        },
                                        i = {
                                                ["<M-p>"] = action_layout.toggle_preview
                                        },
                                },
                                vimgrep_arguments = vimgrep_arguments,
                                preview = {
                                        filesize_limit = 5.0, -- MiB
                                },
                                get_selection_window = function()
                                        local wins = vim.api.nvim_list_wins()
                                        table.insert(wins, 1, vim.api.nvim_get_current_win())
                                        for _, win in ipairs(wins) do
                                                local buf = vim.api.nvim_win_get_buf(win)
                                                if vim.bo[buf].buftype == "" then
                                                        return win
                                                end
                                        end
                                        return 0
                                end,
                        },

                        pickers = {
                                find_files = {
                                        hidden = true,
                                        find_command = { "fd", "--type", "f", "--strip-cwd-prefix", "--exclude", ".git" },
                                },
                                git_files = {
                                        hidden = true,
                                },
                                live_grep = {
                                        hidden = true,
                                },
                                lsp_references = {
                                        trim_text = true,
                                        include_current_line = true,
                                },
                                lsp_definitions = {
                                        trim_text = true,
                                },
                                lsp_type_definitions = {
                                        trim_text = true,
                                },
                                lsp_implementations = {
                                        trim_text = true,
                                },
                                buffers = {
                                        mappings = {
                                                i = {
                                                        ["<c-d>"] = actions.delete_buffer + actions.move_to_top,
                                                }
                                        }
                                }
                        },
                        extensions = {
                                file_browser = {
                                        theme = "dropdown",
                                        hijack_netrw = false,
                                        mappings = {
                                                ["n"] = {
                                                        ["N"] = fb_actions.create,
                                                        ["h"] = fb_actions.goto_parent_dir,
                                                        ["/"] = function()
                                                                vim.cmd "startinsert"
                                                        end,
                                                        ["<C-u>"] = function(prompt_bufnr)
                                                                for _ = 1, 10 do
                                                                        actions.move_selection_previous(prompt_bufnr)
                                                                end
                                                        end,
                                                        ["<C-d>"] = function(prompt_bufnr)
                                                                for _ = 1, 10 do
                                                                        actions.move_selection_next(prompt_bufnr)
                                                                end
                                                        end,
                                                        ["<C-c>"] = "close",
                                                },
                                        },
                                },
                        },
                }

                telescope.load_extension "fzf"
                telescope.load_extension "file_browser"
        end,
}
