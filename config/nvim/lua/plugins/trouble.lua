return {
        "folke/trouble.nvim",
        cmd = { "TroubleToggle", "Trouble" },
        enabled = false,
        keys = {
                { "<leader>xx", "<Cmd>Trouble diagnostics toggle<CR>",              desc = "Diagnostics (Trouble)" },
                { "<leader>xX", "<Cmd>Trouble diagnostics toggle filter.buf=0<CR>", desc = "Buffer diagnostics (Trouble)" },
                { "<leader>cs", "<Cmd>Trouble symbols toggle<CR>",                  desc = "Symbols (Trouble)" },
                { "<leader>cS", "<Cmd>Trouble lsp toggle<CR>",                      desc = "LSP references/definitions/... (Trouble)" },
                { "<leader>xL", "<Cmd>Trouble loclist toggle<CR>",                  desc = "Location list (Trouble)" },
                { "<leader>xQ", "<Cmd>Trouble qflist toggle<CR>",                   desc = "Quickfix list (Trouble)" },
                {
                        "[q",
                        function()
                                if require "trouble".is_open() then
                                        require "trouble".prev { skip_groups = true, jump = true }
                                else
                                        local ok, err = pcall(vim.cmd.cprev)
                                        if not ok then
                                                vim.notify(err, vim.log.levels.ERROR)
                                        end
                                end
                        end,
                        desc = "Previous Trouble/quickfix item",
                },
                {
                        "]q",
                        function()
                                if require "trouble".is_open() then
                                        require "trouble".next { skip_groups = true, jump = true }
                                else
                                        local ok, err = pcall(vim.cmd.cnext)
                                        if not ok then
                                                vim.notify(err, vim.log.levels.ERROR)
                                        end
                                end
                        end,
                        desc = "Next Trouble/quickfix item",
                },
        },
        opts = {
                use_diagnostic_signs = true,
                modes = {
                        lsp = {
                                win = {
                                        position = "right",
                                }
                        }
                }
        },
}
