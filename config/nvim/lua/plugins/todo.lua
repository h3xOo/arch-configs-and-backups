return {
        "folke/todo-comments.nvim",
        cmd = { "TodoTrouble", "TodoTelescope" },
        event = "LazyFile",
        enabled = false,
        opts = function()
                local default_keywords = {
                        FIX = {
                                icon = " ",
                                color = "error",
                                alt = { "FIXME", "BUG", "FIXIT", "ISSUE" },
                        },
                        TODO = { icon = " ", color = "info" },
                        HACK = { icon = " ", color = "warning" },
                        WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
                        PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
                        NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
                        TEST = { icon = "⏲ ", color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
                }

                local keywords = {
                        SEE = {
                                icon = " ",
                                color = "info",
                        },
                }

                keywords = vim.tbl_deep_extend("force", {}, default_keywords, keywords)

                for key, val in pairs(keywords) do
                        local alt = { key:lower(), key }
                        if val.alt then
                                for _, alt_key in ipairs(val.alt) do
                                        alt[#alt + 1] = alt_key
                                        alt[#alt + 1] = alt_key:lower()
                                end
                        end
                        keywords[key].alt = alt
                end

                return {
                        keywords = keywords,
                        highlight = {
                                pattern = {
                                        [[.*<((KEYWORDS)%(\(.{-1,}\))?):]],
                                        [[.*\@((KEYWORDS)%(\(.{-1,}\))?):]],
                                        [[.*\@(KEYWORDS)\s*]],
                                        [[.*<(KEYWORDS)\s*:]],
                                },
                        },
                        search = {
                                pattern = [[\b(KEYWORDS)(\([^\)]*\))?:]],
                        }
                }
        end,
        keys = {
                { "]t",         function() require "todo-comments".jump_next() end,               desc = "Next todo comment" },
                { "[t",         function() require "todo-comments".jump_prev() end,               desc = "Previous todo comment" },
                { "<leader>xt", "<Cmd>Trouble todo toggle<CR>",                                   desc = "Todo (Trouble)" },
                { "<leader>xT", "<Cmd>Trouble todo toggle filter = {tag = {TODO,FIX,FIXME}}<CR>", desc = "Todo/Fix/Fixme (Trouble)" },
                { "<leader>st", "<Cmd>TodoTelescope<CR>",                                         desc = "Todo (Telescope)" },
                { "<leader>sT", "<Cmd>TodoTelescope keywords=TODO,FIX,FIXME<CR>",                 desc = "Todo/Fix/Fixme (Telescope)" },
        },
}
