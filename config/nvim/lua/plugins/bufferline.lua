return {
        "akinsho/bufferline.nvim",
        lazy = false,
        version = "*",
        keys = {
                { "<leader>bp", "<Cmd>BufferLineTogglePin<CR>",            desc = "Toggle pin" },
                { "<leader>bP", "<Cmd>BufferLineGroupClose ungrouped<CR>", desc = "Delete non-pinned buffers" },
                { "<leader>bo", "<Cmd>BufferLineCloseOthers<CR>",          desc = "Delete other buffers" },
                { "<leader>br", "<Cmd>BufferLineCloseRight<CR>",           desc = "Delete buffers to the right" },
                { "<leader>bl", "<Cmd>BufferLineCloseLeft<CR>",            desc = "Delete buffers to the left" },
                { "<S-h>",      "<Cmd>BufferLineCyclePrev<CR>",            desc = "Previous buffer" },
                { "<S-l>",      "<Cmd>BufferLineCycleNext<CR>",            desc = "Next buffer" },
                { "<C-S-h>",    "<Cmd>BufferLineMovePrev<CR>",             desc = "Move previous buffer" },
                { "<C-S-l>",    "<Cmd>BufferLineMoveNext<CR>",             desc = "Move next buffer" },
        },
        opts = {
                options = {
                        diagnostics = "nvim_lsp",
                        always_show_bufferline = false,
                        diagnostics_indicator = function(_, _, diag)
                                local icons = require "stas.icons".diagnostics
                                local ret = (diag.error and icons.Error .. diag.error .. " " or "")
                                    .. (diag.warning and icons.Warn .. diag.warning or "")
                                return vim.trim(ret)
                        end,
                        offsets = {
                                {
                                        filetype = "neo-tree",
                                        text = "Neo-tree",
                                        highlight = "Directory",
                                        text_align = "left",
                                },
                        },
                        ---@param opts bufferline.IconFetcherOpts
                        get_element_icon = function(opts)
                                return require "stas.icons".ft[opts.filetype]
                        end,
                },
        },
        config = function(_, opts)
                require "bufferline".setup(opts)
                -- Fix bufferline when restoring a session
                vim.api.nvim_create_autocmd({ "BufAdd", "BufDelete" }, {
                        callback = function()
                                vim.schedule(function()
                                        pcall(nvim_bufferline)
                                end)
                        end,
                })
        end,
}
