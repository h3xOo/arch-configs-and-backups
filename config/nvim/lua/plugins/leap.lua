return {
        {
                "ggandor/flit.nvim",
                lazy = false,
                opts = { labeled_modes = "nxvo" },
        },
        {
                "ggandor/leap.nvim",
                lazy = false,
                opts = {
                        equivalence_classes = {
                                " \t\r\n",
                                "([{", ")]}",
                                [['"`]],
                                "aäàáâãāą",
                                "cć",
                                "dḍ",
                                "eëéèêēę",
                                "gǧğ",
                                "hḥḫ",
                                "iïīíìîı",
                                "lł",
                                "nñń",
                                "oōó",
                                "sṣšßś",
                                "tṭ",
                                "uúûüűū",
                                "zẓżź",
                        },
                        highlight_unlabeled_phase_one_targets = true,
                        preview_filter = function(ch0, ch1, ch2)
                                return not (
                                        ch1:match("%s") or
                                        ch0:match("%w") and ch1:match("%w") and ch2:match("%w")
                                )
                        end,
                },
                config = function(_, opts)
                        vim.g.vscode = true
                        local leap = require "leap"
                        if vim.g.colors_name ~= "nord" then
                                leap.init_highlight(true)
                        end
                        local leap_user = require "leap.user"
                        for k, v in pairs(opts) do
                                leap.opts[k] = v
                        end
                        -- This breaks <CR> in quickfix list. Either use commented version,
                        -- or use autocmd for remapping <CR> to <CR> in quickfix filetype.
                        leap_user.set_repeat_keys("<CR>", "<BS>")
                        -- leap_user.set_repeat_keys("<CR>", "<BS>", { modes = { "x", "o" } })
                        local use_default_mappings = true
                        if use_default_mappings then
                                vim.keymap.set({ "n", "x", "o" }, "s", "<Plug>(leap-forward)", { desc = "Leap forward" })
                                vim.keymap.set({ "n", "x", "o" }, "S", "<Plug>(leap-backward)",
                                        { desc = "Leap backward" })
                                vim.keymap.set({ "n", "x", "o" }, "gs", "<Plug>(leap-from-window)",
                                        { desc = "Leap from window" })
                        else
                                vim.keymap.set("n", "s", "<Plug>(leap)", { desc = "Leap anywhere" })
                                vim.keymap.set("n", "S", "<Plug>(leap-from-window)", { desc = "Leap from window" })
                                vim.keymap.set({ "x", "o" }, "s", "<Plug>(leap-forward)", { desc = "Leap forward" })
                                vim.keymap.set({ "x", "o" }, "S", "<Plug>(leap-backward)", { desc = "Leap backward" })
                        end

                        -- Incremental treesitter selection
                        vim.keymap.set({ "n", "x", "o" }, "ga", function()
                                require "leap.treesitter".select()
                        end, { desc = "Leap incremental selection" })

                        -- Linewise
                        vim.keymap.set({ "n", "x", "o" }, "gA",
                                [[V<Cmd>lua require "leap.treesitter".select()<CR>]],
                                { desc = "Leap incremental selection (linewise)", silent = true })

                        -- If you're using the default mappings (`gs` for multi-window mode), you can
                        -- map e.g. `gS` here.
                        vim.keymap.set({ "n", "x", "o" }, "gS", function()
                                require "leap.remote".action()
                        end, { desc = "Leap spooky" })
                end,
        },

        { "tpope/vim-repeat", lazy = false },
}
