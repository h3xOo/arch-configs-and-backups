return {
        "nvim-neo-tree/neo-tree.nvim",
        dependencies = {
                "MunifTanjim/nui.nvim",
                lazy = true
        },
        keys = {
                { "<C-h>", "<Cmd>Neotree toggle<CR>", silent = true, noremap = true, desc = "Toggle neotree" },
        },
        branch = "v3.x",
        cmd = "Neotree",
        deactivate = function()
                vim.cmd "Neotree close"
        end,
        init = function()
                -- FIX: use `autocmd` for lazy-loading neo-tree instead of directly requiring it,
                -- because `cwd` is not set up properly.
                vim.api.nvim_create_autocmd("BufEnter", {
                        group = vim.api.nvim_create_augroup("Neotree_start_directory", { clear = true }),
                        desc = "Start Neo-tree with directory",
                        once = true,
                        callback = function()
                                if package.loaded["neo-tree"] then
                                        return
                                else
                                        local stats = vim.uv.fs_stat(vim.fn.argv(0))
                                        if stats and stats.type == "directory" then
                                                require "neo-tree"
                                        end
                                end
                        end,
                })
        end,
        opts = {
                close_if_last_window = true,
                sources = { "filesystem", "buffers", "git_status" },
                open_files_do_not_replace_types = { "terminal" },
                filesystem = {
                        bind_to_cwd = false,
                        follow_current_file = {
                                enabled = true,
                        },
                        filtered_items = {
                                hide_by_name = {
                                        "node_modules",
                                        "build",
                                },
                        },
                        use_libuv_file_watcher = true,
                },
                window = {
                        mappings = {
                                ["<space>"] = "none",
                                ["h"] = {
                                        function(state)
                                                local node = state.tree:get_node()
                                                if node.type == "directory" and node:is_expanded() then
                                                        require "neo-tree.sources.filesystem".toggle_directory(state,
                                                                node)
                                                else
                                                        require "neo-tree.ui.renderer".focus_node(state,
                                                                node:get_parent_id())
                                                end
                                        end,
                                        desc = "out",
                                },
                                ["f"] = "fuzzy_finder",
                                ["/"] = "filter_on_submit",
                                ["l"] = {
                                        function(state)
                                                local node = state.tree:get_node()
                                                if require "neo-tree.utils".is_expandable(node) then
                                                        state.commands["toggle_node"](state)
                                                else
                                                        state.commands["open"](state)
                                                        vim.cmd "Neotree reveal"
                                                end
                                        end,
                                        desc = "preview file/toggle directory"
                                },
                        },
                },
                default_component_configs = {
                        indent = {
                                with_expanders = true, -- if nil and file nesting is enabled, will enable expanders
                                expander_collapsed = "",
                                expander_expanded = "",
                                expander_highlight = "NeoTreeExpander",
                        },
                        git_status = {
                                symbols = {
                                        unstaged = "󰄱",
                                        staged = "󰱒",
                                },
                        },
                },
        },
}
