return {
        {
                "nvim-treesitter/nvim-treesitter",
                version = false,
                event = { "LazyFile", "VeryLazy" },
                init = function(plugin)
                        require "lazy.core.loader".add_to_rtp(plugin)
                        require "nvim-treesitter.query_predicates"
                end,
                lazy = vim.fn.argc(-1) == 0,
                cmd = { "TSUpdateSync", "TSUpdate", "TSInstall" },
                build = function()
                        require "nvim-treesitter.install".update { with_sync = false } ()
                end,
                opts = {
                        ignore_install = { "latex" },
                        highlight = {
                                enable = true,
                                disable = { "latex" },
                        },
                        ensure_installed = {
                                "bash",
                                "c",
                                "cpp",
                                "lua",
                                "markdown",
                                "markdown_inline",
                                "python",
                                "query",
                                "regex",
                                "rust",
                                "vim",
                                "vimdoc",
                        },
                        sync_install = false,
                        auto_install = false,
                },
                config = function(_, opts)
                        require "nvim-treesitter.configs".setup(opts)
                end,
        },
        {
                "nvim-treesitter/nvim-treesitter-context",
                event = { "LazyFile", "VeryLazy" },
                opts = { mode = "cursor", max_lines = 3, enable = true },
                keys = {
                        {
                                "[c",
                                function()
                                        require "treesitter-context".go_to_context(vim.v.count1)
                                end,
                                silent = true,
                                desc = "Jump to context"
                        },
                }
        },
        {
                "windwp/nvim-ts-autotag",
                event = "LazyFile",
                opts = {},
        },
}
