return {
        "hrsh7th/nvim-cmp",
        name = "nvim-cmp",
        event = "InsertEnter",
        version = false,
        dependencies = {
                {
                        "garymjr/nvim-snippets",
                        opts = {
                                extended_filetypes = {
                                        bash = { "shelldoc" },
                                        c = { "cdoc" },
                                        cpp = { "c", "cppdoc", "cdoc" },
                                        cs = { "csharpdoc" },
                                        java = { "javadoc" },
                                        javascript = { "jsdoc" },
                                        javascriptreact = { "jsdoc" },
                                        kotlin = { "kdoc" },
                                        lua = { "luadoc" },
                                        php = { "phpdoc" },
                                        python = { "pydoc" },
                                        ruby = { "rdoc", "rails" },
                                        rust = { "rustdoc" },
                                        sh = { "shelldoc" },
                                        shell = { "shelldoc" },
                                        shellscript = { "shelldoc" },
                                        typescript = { "tsdoc", "jsdoc", "javascript" },
                                        typescriptreact = { "tsdoc", "jsdoc", "javascript" },
                                        zsh = { "shelldoc" },
                                },
                        },
                        keys = {
                                {
                                        "<Tab>",
                                        function()
                                                return vim.snippet.active { direction = 1 } and
                                                    "<Cmd>lua vim.snippet.jump(1)<CR>" or "<Tab>"
                                        end,
                                        expr = true,
                                        silent = true,
                                        desc = "Snippets jump next",
                                        mode = { "i", "s" },
                                },
                                {
                                        "<S-Tab>",
                                        function()
                                                return vim.snippet.active { direction = -1 } and
                                                    "<Cmd>lua vim.snippet.jump(-1)<CR>" or "<S-Tab>"
                                        end,
                                        expr = true,
                                        silent = true,
                                        desc = "Snippets jump previous",
                                        mode = { "i", "s" },
                                },
                        },
                },
                "https://codeberg.org/FelipeLema/cmp-async-path",
                "hrsh7th/cmp-buffer",
                "hrsh7th/cmp-nvim-lua",
                "hrsh7th/cmp-nvim-lsp",
                "hrsh7th/cmp-nvim-lsp-signature-help",
        },
        opts = function()
                local cmp = require "cmp"
                local defaults = require "cmp.config.default" ()
                return {
                        completion = {
                                completeopt = "menu,menuone,noinsert,preview",
                        },
                        snippet = {
                                expand = function(args)
                                        vim.snippet.expand(args.body)
                                end,
                        },
                        mapping = cmp.mapping.preset.insert {
                                ["<C-n>"] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
                                ["<C-p>"] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },
                                ["<C-d>"] = cmp.mapping.scroll_docs(4),
                                ["<C-u>"] = cmp.mapping.scroll_docs(-4),
                                ["<C-Space>"] = cmp.mapping.complete(),
                                ["<C-e>"] = cmp.mapping.abort(),
                                ["<Tab>"] = cmp.mapping.confirm { select = true },
                                ["<C-g>"] = function()
                                        if cmp.visible_docs() then
                                                cmp.close_docs()
                                        else
                                                cmp.open_docs()
                                        end
                                end,
                        },
                        sources = cmp.config.sources {
                                { name = "nvim_lsp" },
                                { name = "nvim_lsp_signature_help" },
                                { name = "nvim_lua" },
                                { name = "snippets" },
                                {
                                        name = "async_path",
                                        option = { trailing_slash = true }
                                },
                                {
                                        name = "buffer",
                                        option = {
                                                keyword_pattern = [[\k\+]],
                                                get_bufnrs = function()
                                                        local bufs = {}
                                                        for _, buf in ipairs(vim.api.nvim_list_bufs()) do
                                                                local byte_size = vim.api.nvim_buf_get_offset(buf,
                                                                        vim.api.nvim_buf_line_count(buf))
                                                                if vim.api.nvim_buf_is_loaded(buf) and byte_size <= 10 * 1024 * 1024 then
                                                                        bufs[buf] = true
                                                                end
                                                        end
                                                        return vim.tbl_keys(bufs)
                                                end,
                                        }
                                },
                        },
                        formatting = {
                                fields = { "abbr", "kind", "menu" },
                                format = function(_, item)
                                        local icons = require "stas.icons".kinds
                                        if icons[item.kind] then
                                                item.kind = icons[item.kind] .. item.kind
                                        end

                                        local widths = {
                                                abbr = vim.g.cmp_widths and vim.g.cmp_widths.abbr or 40,
                                                menu = vim.g.cmp_widths and vim.g.cmp_widths.menu or 30,
                                        }

                                        for key, width in pairs(widths) do
                                                if item[key] and vim.fn.strdisplaywidth(item[key]) > width then
                                                        item[key] = vim.fn.strcharpart(item[key], 0, width - 1) .. "..."
                                                end
                                        end

                                        return item
                                end,
                        },
                        sorting = defaults.sorting,
                }
        end,
        config = function(_, opts)
                table.insert(opts.sorting.comparators, 1, function(...)
                        return require "cmp_buffer":compare_locality(...)
                end)
                require "cmp".setup(opts)
        end,
}
