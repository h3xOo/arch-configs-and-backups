return {
        {
                "iamcco/markdown-preview.nvim",
                cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
                build = function()
                        require "lazy".load { plugins = { "markdown-preview.nvim" } }
                        vim.fn["mkdp#util#install"]()
                end,
                keys = {
                        {
                                "<leader>mp",
                                ft = "markdown",
                                "<Cmd>MarkdownPreviewToggle<CR>",
                                desc = "Toggle markdown web preview",
                                silent = true,
                                noremap = true,
                        },
                },
                config = function()
                        vim.cmd [[do FileType]]
                end,
        },
        {
                "MeanderingProgrammer/render-markdown.nvim",
                cmd = "RenderMarkdown",
                opts = {
                        code = {
                                sign = false,
                                width = "block",
                                right_pad = 1,
                        },
                        heading = {
                                sign = false,
                                icons = {},
                        },
                },
                keys = {
                        {
                                "<leader>mr",
                                "<Cmd>RenderMarkdown toggle<CR>",
                                desc = "Toggle markdown conceal",
                                silent = true,
                                noremap = true
                        },
                },
                ft = { "markdown", "norg", "rmd", "org" },
        },
}
