return {
        "lervag/vimtex",
        lazy = false,
        init = function()
                vim.g.tex_comment_nospell = 1
                vim.g.tex_flavor = "latex"
                vim.g.vimtex_compiler_method = "latexmk"
                vim.g.vimtex_imaps_enabled = 0
                vim.g.vimtex_mappings_disable = { ["n"] = { "K" } }
                vim.g.vimtex_quickfix_method = "pplatex"
                vim.g.vimtex_view_method = "zathura"
        end,
}
