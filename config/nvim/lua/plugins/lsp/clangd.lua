return {
        {
                "p00f/clangd_extensions.nvim",
                ft = { "c", "cpp" },
                lazy = true,
                opts = {
                        ast = {
                                role_icons = {
                                        type = "",
                                        declaration = "",
                                        expression = "",
                                        specifier = "",
                                        statement = "",
                                        ["template argument"] = "",
                                },
                                kind_icons = {
                                        Compound = "",
                                        Recovery = "",
                                        TranslationUnit = "",
                                        PackExpansion = "",
                                        TemplateTypeParm = "",
                                        TemplateTemplateParm = "",
                                        TemplateParamObject = "",
                                },
                        },
                },
        },
        {
                "nvim-cmp",
                opts = function(_, opts)
                        table.insert(opts.sorting.comparators, 1, require "clangd_extensions.cmp_scores")
                end,
        },
}
