return {
        {
                "mrcjkb/rustaceanvim",
                version = "^5",
                init = function()
                        vim.g.rustaceanvim = {}
                end,
                opts = function()
                        return {
                                server = {
                                        on_attach = require "stas.lsp".on_attach,
                                        default_settings = {
                                                ["rust-analyzer"] = {
                                                        capabilities = require "stas.lsp".make_capabilities(),
                                                        cargo = {
                                                                allFeatures = true,
                                                                loadOutDirsFromCheck = true,
                                                                buildScripts = {
                                                                        enable = true,
                                                                },
                                                        },
                                                        checkOnSave = true,
                                                        procMacro = {
                                                                enable = true,
                                                                ignored = {
                                                                        ["async-trait"] = { "async_trait" },
                                                                        ["napi-derive"] = { "napi" },
                                                                        ["async-recursion"] = { "async_recursion" },
                                                                },
                                                        },
                                                },
                                        },
                                },
                        }
                end,
                config = function(_, opts)
                        vim.g.rustaceanvim = vim.tbl_deep_extend("keep", vim.g.rustaceanvim, opts or {})
                end,
        },
        {
                "nvim-cmp",
                dependencies = {
                        {
                                "Saecki/crates.nvim",
                                event = { "BufRead Cargo.toml" },
                                opts = {
                                        completion = {
                                                cmp = { enabled = true },
                                        },
                                },
                        },
                },
                opts = function(_, opts)
                        table.insert(opts.sources, { name = "crates", group_index = 1 })
                end,
        }
}
