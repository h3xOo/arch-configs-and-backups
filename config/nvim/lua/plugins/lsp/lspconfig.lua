---@diagnostic disable: undefined-field
return {
        "neovim/nvim-lspconfig",
        dependencies = {
                "hrsh7th/cmp-nvim-lsp",
                "williamboman/mason.nvim",
                "williamboman/mason-lspconfig.nvim",
        },
        event = "LazyFile",
        config = function()
                local lspconfig = require "lspconfig"

                local on_attach = require "stas.lsp".on_attach
                local keymap_opts = require "stas.lsp".keymap_opts
                local key_set = vim.keymap.set

                local capabilities = require "stas.lsp".make_capabilities()

                local root_pattern = require "lspconfig.util".root_pattern

                local git_ancestor = function(startpath)
                        return vim.fs.dirname(vim.fs.find(".git", { path = startpath, upward = true })[1])
                end

                lspconfig.bashls.setup {
                        on_attach = on_attach,
                        capabilities = capabilities,
                        filetypes = { "sh", "zsh", "bash" },
                }

                lspconfig.clangd.setup {
                        cmd = {
                                "clangd",
                                "--all-scopes-completion",
                                "--background-index",
                                "--completion-style=detailed",
                                "--clang-tidy",
                                "--enable-config",
                                "--function-arg-placeholders",
                                "--header-insertion=never",
                                "--header-insertion-decorators",
                                "-j=4",
                                "--malloc-trim",
                                "--offset-encoding=utf-16",
                                "--parse-forwarding-functions",
                                "--pch-storage=memory",
                        },
                        capabilities = require "stas.lsp".make_clangd_capabilities(),
                        on_attach = function(client, bufnr)
                                on_attach(client, bufnr)
                                key_set("n", "gch", "<Cmd>ClangdSwitchSourceHeader<CR>",
                                        keymap_opts("Clangd switch source header", bufnr))
                        end,
                        filetypes = { "cpp", "c" },
                        single_file_support = true,
                        root_dir = function(fname)
                                return root_pattern(
                                            "Makefile",
                                            "configure.ac",
                                            "configure.in",
                                            "config.h.in",
                                            "meson.build",
                                            "meson_options.txt",
                                            "build.ninja",
                                            ".clangd"
                                    )(fname)
                                    or root_pattern("compile_commands.json", "compile_flags.txt")(fname)
                                    or git_ancestor(fname)
                        end,
                        init_options = {
                                usePlaceholders = true,
                                completeUnimported = true,
                                clangdFileStatus = true,
                        },
                }

                -- lspconfig.cssls.setup {
                --         on_attach = on_attach,
                --         capabilities = capabilities,
                --         single_file_support = true,
                -- }
                --
                -- lspconfig.eslint.setup {
                --         capabilities = capabilities,
                --         on_attach = function(_, bufnr)
                --                 vim.api.nvim_create_autocmd("BufWritePre", {
                --                         buffer = bufnr,
                --                         command = "EslintFixAll",
                --                 })
                --         end,
                --         settings = {
                --                 workingDirectories = { mode = "auto" },
                --         },
                -- }
                --
                -- lspconfig.html.setup {
                --         on_attach = on_attach,
                --         capabilities = capabilities,
                --         single_file_support = true,
                -- }

                lspconfig.lua_ls.setup {
                        on_attach = on_attach,
                        capabilities = capabilities,
                        single_file_support = true,
                        on_init = function(client)
                                if client.workspace_folders then
                                        local stat = vim.uv.fs_stat
                                        local path = client.workspace_folders[1].name
                                        if stat(path .. "/.luarc.json") or stat(path .. "/.luarc.jsonc") then
                                                return
                                        end
                                end

                                client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings.Lua, {
                                        runtime = {
                                                version = "LuaJIT",
                                        },
                                        workspace = {
                                                checkThirdParty = false,
                                                library = {
                                                        vim.env.VIMRUNTIME,
                                                },
                                        },
                                })
                        end,
                        settings = {
                                Lua = {
                                        diagnostics = {
                                                globals = { "vim" },
                                        },
                                        format = {
                                                enable = true,
                                                defaultConfig = {
                                                        indent_style = "space",
                                                        quote_style = "double",
                                                        indent_size = "8",
                                                        tab_width = "8",
                                                        table_separator_style = "comma",
                                                        trailing_table_separator = "smart",
                                                        space_before_function_open_parenthesis = false,
                                                        align_continuous_rect_table_field = "false",
                                                        align_array_table = "none",
                                                        align_continuous_assign_statement = "false",
                                                        call_arg_parentheses = "remove",
                                                },
                                        },
                                },
                        },
                }

                -- lspconfig.neocmake.setup {
                --         on_attach = on_attach,
                --         capabilities = capabilities,
                --         single_file_support = true,
                -- }

                lspconfig.pyright.setup {
                        on_attach = on_attach,
                        capabilities = capabilities,
                        single_file_support = true,
                        settings = {
                                python = {
                                        analysis = {
                                                autoImportCompletions = true,
                                                autoSearchPaths = true,
                                                diagnosticMode = "openFilesOnly",
                                                useLibraryCodeForTypes = true,
                                                typeCheckingMode = "standard",
                                                disableOrganizeImports = true,
                                        },
                                },
                        },
                }

                lspconfig.ruff.setup {
                        on_attach = on_attach,
                        capabilities = capabilities,
                        init_options = {
                                settings = {
                                        logLevel = "error",
                                        organizeImports = true,
                                        showSyntaxErrors = true,
                                        lint = {
                                                enable = true,
                                        },
                                },
                        }
                }

                lspconfig.texlab.setup {
                        filetypes = { "tex", "plaintex", "bib" },
                        on_attach = function(client, bufnr)
                                on_attach(client, bufnr)
                                key_set("n", "<leader>K", "<plug>(vimtex-doc-package)",
                                        keymap_opts("Hover", bufnr))
                        end,
                        capabilities = capabilities,
                        single_file_support = true,
                        settings = {
                                texlab = {
                                        latexFormatter = "latexindent",
                                        latexindent = {
                                                modifyLineBreaks = true,
                                                replacement = "-rr",
                                        },
                                        chktex = {
                                                onOpenAndSave = true,
                                                onEdit = false,
                                                additionalArgs = {
                                                        "-v2",
                                                        "-x",
                                                },
                                        },
                                        diagnosticsDelay = 100,
                                },
                        },
                }

                -- lspconfig.tinymist.setup {
                --         on_attach = on_attach,
                --         capabilities = capabilities,
                --         single_file_support = true,
                --         offset_encoding = "utf-8",
                --         root_dir = function(name)
                --                 return git_ancestor(name) or vim.fn.getcwd()
                --         end,
                --         settings = {
                --                 exportPdf = "onType",
                --                 outputPath = "$root/target/$dir/$name",
                --                 formatterMode = "typstfmt",
                --         },
                -- }
                --
                -- lspconfig.zls.setup {
                --         on_attach = on_attach,
                --         capabilities = capabilities,
                --         single_file_support = true,
                -- }
        end,
}
