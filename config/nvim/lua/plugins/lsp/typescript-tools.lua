return {
        "pmizio/typescript-tools.nvim",
        ft = { "javascript", "javascriptreact", "javascript.jsx", "typescript", "typescriptreact", "typescript.tsx" },
        enabled = false,
        dependencies = { "neovim/nvim-lspconfig" },
        opts = function()
                return {
                        on_attach = require "stas.lsp".on_attach,
                        settings = {
                                tsserver_file_preferences = {
                                        includeInlayParameterNameHints = "all",
                                        includeCompletionsForModuleExports = true,
                                        quotePreference = "auto",
                                },
                                tsserver_format_options = {
                                        allowIncompleteCompletions = false,
                                        allowRenameOfImportPath = false,
                                },
                                tsserver_plugins = {
                                        "@styled/typescript-styled-plugin",
                                },
                        },
                }
        end,
}
