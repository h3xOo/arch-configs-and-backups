return {
        {
                "folke/lazydev.nvim",
                ft = "lua",
                cmd = "LazyDev",
                opts = {
                        library = {
                                { path = "${3rd}/luv/library", words = { "vim%.uv" } },
                        },
                },
        },
        {
                "nvim-cmp",
                opts = function(_, opts)
                        table.insert(opts.sources, {
                                name = "lazydev",
                                group_index = 0,
                        })
                end,
        },
}
