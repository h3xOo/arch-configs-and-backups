return {
        {
                "williamboman/mason.nvim",
                cmd = "Mason",
                keys = { { "<leader>cm", "<cmd>Mason<cr>", desc = "Mason" } },
                build = ":MasonUpdate",
                opts = {},
        },
        {
                "williamboman/mason-lspconfig.nvim",
                opts = {
                        automatic_installation = false,
                        -- ensure_installed = {
                        --         "basedpyright",
                        --         "bashls",
                        --         "clangd",
                        --         "cssls",
                        --         "eslint",
                        --         "html",
                        --         "lua_ls",
                        --         "neocmake",
                        --         "ruff",
                        --         "texlab",
                        --         "tinymist",
                        --         "zls",
                        -- },
                },
        },
}
