local function get_colorscheme(name)
        local full_path = "plugins._colorschemes." .. name
        return require(full_path)
end

return get_colorscheme("gruvbox-material")
