if vim.loader then
        vim.loader.enable()
end

local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.uv.fs_stat(lazypath) then
        local lazyrepo = "https://github.com/folke/lazy.nvim.git"
        local out = vim.fn.system { "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath }
        if vim.v.shell_error ~= 0 then
                vim.api.nvim_echo({
                        { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
                        { out,                            "WarningMsg" },
                        { "\nPress any key to exit..." },
                }, true, {})
                vim.fn.getchar()
                os.exit(1)
        end
end

vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

require "stas"
require "lazy".setup({
        { import = "plugins" },
        { import = "plugins.lsp" },
}, {
        --[[ ui = {
                backdrop = 100,
        }, ]]
        defaults = {
                lazy = false,
        },
        install = {
                colorscheme = { "gruvbox", "nord", "retrobox", "vim" },
        },
        checker = {
                enabled = false,
        },
        change_detection = {
                notify = false,
        },
        rocks = {
                enabled = false,
        },
        performance = {
                rtp = {
                        reset = false,
                        -- disable some rtp plugins
                        -- disabled_plugins = {
                        --         "gzip",
                        --         "netrwPlugin",
                        --         "tarPlugin",
                        --         "tohtml",
                        --         "tutor",
                        --         "zipPlugin",
                        -- },
                },
        },
})
