-- Pull in the wezterm API
local wezterm = require "wezterm"

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
        config = wezterm.config_builder()
end

config.font = wezterm.font "JetBrainsMono NF"
config.font_size = 13
config.enable_tab_bar = true
config.window_close_confirmation = "NeverPrompt"

config.color_scheme = "Gruvbox dark, soft (base16)"

config.window_padding = {
        left = 4,
        right = 0,
        top = 4,
        bottom = 2,
}

local gpus = wezterm.gui.enumerate_gpus()

config.front_end = "WebGpu"
config.webgpu_preferred_adapter = gpus[2]

config.disable_default_key_bindings = true

config.keys = {
        {
                key = "r",
                mods = "CTRL|SHIFT",
                action = wezterm.action.Search { CaseSensitiveString = "" }
        },
        {
                key = "o",
                mods = "CTRL|SHIFT",
                action = wezterm.action.OpenLinkAtMouseCursor,
        },
        {
                key = "q",
                mods = "CTRL|SHIFT",
                action = wezterm.action.CloseCurrentTab { confirm = false }
        },
        {
                key = "k",
                mods = "CTRL",
                action = wezterm.action.ScrollByPage(-0.5),
        },
        {
                key = "j",
                mods = "CTRL",
                action = wezterm.action.ScrollByPage(0.5),
        },
        {
                key = "k",
                mods = "CTRL|SHIFT",
                action = wezterm.action.ScrollByLine(-1),
        },
        {
                key = "j",
                mods = "CTRL|SHIFT",
                action = wezterm.action.ScrollByLine(1),
        },
        {
                key = "c",
                mods = "CTRL|SHIFT",
                action = wezterm.action.CopyTo "ClipboardAndPrimarySelection",
        },
        {
                key = "v",
                mods = "CTRL|SHIFT",
                action = wezterm.action.PasteFrom "Clipboard",
        },
        {
                key = "p",
                mods = "CTRL",
                action = wezterm.action.ActivateCommandPalette,
        },
        {
                key = "L",
                mods = "CTRL",
                action = wezterm.action.ShowDebugOverlay
        },
}

config.use_fancy_tab_bar = false
config.hide_tab_bar_if_only_one_tab = true
-- config.tab_bar_at_bottom = true
config.tab_max_width = 100

config.default_gui_startup_args = { "start", "--cwd", "." }

wezterm.on("gui-startup", function()
        local tab, pane, window = wezterm.mux.spawn_window {}
        window:gui_window():maximize()
end)

config.hide_mouse_cursor_when_typing = true
config.scrollback_lines = 4096

-- and finally, return the configuration to wezterm
return config
