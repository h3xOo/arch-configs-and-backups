#!/bin/zsh

# Sanity check
[[ -z "${PATH}" ]] && export PATH="/bin:/sbin:/usr/bin"

# Add $HOME/.local/bin to PATH
[[ -d "${HOME}/.local/bin" ]] && export PATH="${PATH}:${HOME}/.local/bin"

# Add ccache to PATH
[[ -d "/usr/lib/ccache/bin" ]] && export PATH="/usr/lib/ccache/bin:${PATH}"

# XDG Paths
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"

# ZDOTDIR
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"

# ZSH Vi Mode
export KEYTIMEOUT=1

# Mozilla Wayland
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1

# Programs
export BROWSER="firefox"
export CC="gcc"
export CXX="g++"
export EDITOR="nvim"
export IMAGE="sxiv"
export MANPAGER="less -R --use-color -Dd+r -Du+b"
export MANROFFOPT="-P -c"
export OPENER="xdg-open"
export PAGER="less"
export READER="zathura"
export TERMINAL="foot"
export VIDEO="mpv"
export VISUAL="nvim"
export WM="river"

# export RUSTC_WRAPPER=sccache

# For Intellij CE
export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT="MToolkit"

# Debuginfod
export DEBUGINFOD_URLS="https://debuginfod.archlinux.org/ https://debuginfod.elfutils.org/"

### Other paths/Clean up
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export DOOMWADDIR="${XDG_DATA_HOME}/doom"
export DOT_SAGE="${XDG_CONFIG_HOME}/sage"
export FFMPEG_DATADIR="${XDG_CONFIG_HOME}/ffmpeg"
export FZF_DEFAULT_OPTS_FILE="${XDG_CONFIG_HOME}/fzfrc"
export GDBHISTFILE="${XDG_CACHE_HOME}/gdbhistory"
export GRADLE_USER_HOME="${XDG_DATA_HOME}/gradle"
export GOPATH="${XDG_DATA_HOME}/go"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc"
export GTK_RC_FILES="${XDG_CONFIG_HOME}/gtk-1.0/gtkrc"
export HISTFILE="${XDG_DATA_HOME}/history"
export IPYTHONDIR="${XDG_CONFIG_HOME}/ipython"
export JUPYTER_CONFIG_DIR="${XDG_CONFIG_HOME}/jupyter"
# export LADYBIRD_SOURCE_DIR="${HOME}/src/ladybird"
export LESSHISTFILE="-"
export NODE_REPL_HISTORY="${XDG_DATA_HOME}/node_repl_history"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"
export OCTAVE_HISTFILE="${XDG_CACHE_HOME}/octave-hsts"
export OCTAVE_SITE_INITFILE="${XDG_CONFIG_HOME}/octave/octaverc"
export R_ENVIRON_USER="${XDG_CONFIG_HOME}/R/Renviron"
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
# export SERENITY_SOURCE_DIR="${HOME}/src/serenity"
export TEXMFCONFIG="${XDG_CONFIG_HOME}/texlive/texmf-config"
export TEXMFHOME="${XDG_DATA_HOME}/texmf"
export TEXMFVAR="${XDG_CACHE_HOME}/texlive/texmf-var"
export TMUX_TMPDIR="${XDG_RUNTIME_DIR}"
export WGETRC="${XDG_CONFIG_HOME}/wgetrc"

# Append cargo bin to PATH
[[ -d "${CARGO_HOME}/bin" ]] && export PATH="${PATH}:${CARGO_HOME}/bin"

[[ -d "${XDG_DATA_HOME}/npm/bin" ]] && export PATH="${PATH}:${XDG_DATA_HOME}/npm/bin"

case "${COLORTERM}" in
    truecolor)
        # do not overwrite
        ;;
    *)
        export COLORTERM="yes"
        ;;
esac
