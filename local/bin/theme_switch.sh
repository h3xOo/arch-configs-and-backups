#!/bin/sh

CONFIG_DIR=${XDG_CONFIG_HOME:-${HOME}/.config}
ZSH_DIR=${ZDOTDIR:-${HOME}}
CURRENT_GTK_THEME=`gsettings get org.gnome.desktop.interface gtk-theme`

select_theme() {
        case $1 in
                daytime|light)
                        if [[  "${CURRENT_GTK_THEME}" = "'WhiteSur-Light'" ]]; then
                                exit 0
                        fi
                        sed 's/dark/light/g' -i ${CONFIG_DIR}/foot/foot.ini ${CONFIG_DIR}/nvim/lua/stas/base.lua ${CONFIG_DIR}/fzfrc
                        sed 's/gruvbox-dark/gruvbox-light/g' -i ${CONFIG_DIR}/bat/config
                        sed 's/dark_theme/light_theme/g' -i ${CONFIG_DIR}/fuzzel/fuzzel.ini
                        gsettings set org.gnome.desktop.interface gtk-theme 'WhiteSur-Light'
                        ;;
                night|dark)
                        if [[  "${CURRENT_GTK_THEME}" = "'WhiteSur-Dark'" ]]; then
                                exit 0
                        fi
                        sed 's/light/dark/g' -i ${CONFIG_DIR}/foot/foot.ini ${CONFIG_DIR}/nvim/lua/stas/base.lua ${CONFIG_DIR}/fzfrc
                        sed 's/gruvbox-light/gruvbox-dark/g' -i ${CONFIG_DIR}/bat/config
                        sed 's/light_theme/dark_theme/g' -i ${CONFIG_DIR}/fuzzel/fuzzel.ini
                        gsettings set org.gnome.desktop.interface gtk-theme 'WhiteSur-Dark'
                        ;;
                *)
                        echo -e "\033[1;31mUnknown theme: '${1}'.\033[0;37m"
                        exit 1
                        ;;
        esac

        nwg-look -x >/dev/null 2>&1
}

if [[  "${CURRENT_GTK_THEME}" = "'WhiteSur-Dark'" ]]; then
        select_theme "light"
elif [[ "${CURRENT_GTK_THEME}" = "'WhiteSur-Light'" ]]; then
        select_theme "dark"
else
	echo "Unknown gtk theme: ${CURRENT_GTK_THEME}"
	exit 1
fi
