# Artix personal configs and backups

It is recommended to use `zsh` (Z shell) as user shell.

Personal config files and some system backups to make it easier to backup if needed.

To reinstall all packages from file to new Artix first:
- install `artix-archlinux-support` and `paru`,
- {re}install all packages with `cat etc/pkglist.txt | paru -Syu -`.

Fonts should be cached with `fc-cache -fv`.
